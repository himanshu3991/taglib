package com.enforcers.lib.enforcerslib.logger.bean.common;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class VideoDetails {

    private String contentQuality;

    private String playerUsed;

    private Long percentageVideo;

    private Boolean isAutoPlay;

    private Long videoStartTimeStamp;


}
