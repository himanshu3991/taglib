package com.enforcers.lib.enforcerslib.logger.bean.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ContentDetails {

    private String contentId;

    private String contentType;

    private String title;

    private String contentMood;

    private String genre;

    private String cpName;

    private String showName;
}
