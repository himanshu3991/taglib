package com.enforcers.lib.enforcerslib.logger.bean.v1.page;

import com.enforcers.lib.enforcerslib.logger.bean.common.LogType;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PageViewMessage {

    private String sessionId;

    private String pageName;

    private String pageUrl;

    private Long h;

    private final LogType logType =LogType.PAGE_VIEW;
}
