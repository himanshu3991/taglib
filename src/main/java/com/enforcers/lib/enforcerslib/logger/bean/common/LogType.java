package com.enforcers.lib.enforcerslib.logger.bean.common;

public enum LogType {

    VALIDATION,API,RE_DIRECTION, MASTER, PAGE_VIEW, EVENT
}
