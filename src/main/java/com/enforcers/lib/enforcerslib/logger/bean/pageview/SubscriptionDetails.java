package com.enforcers.lib.enforcerslib.logger.bean.pageview;

import com.enforcers.lib.enforcerslib.logger.bean.pageview.constant.Status;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SubscriptionDetails {

    private Long startDate;

    private Long expiryDate;

    private Status status;

    private String billingMode;

    private String subsPartner;

    private String offerPartner;

    private String billingPartner;
}
