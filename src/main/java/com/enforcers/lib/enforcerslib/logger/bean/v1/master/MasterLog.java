package com.enforcers.lib.enforcerslib.logger.bean.v1.master;

import com.enforcers.lib.enforcerslib.logger.bean.common.LogType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MasterLog {

    private Map<String,String> headers;

    private Map<String,String> params;

    private String sessionId;

    private Long h;

    private final LogType logType =LogType.MASTER;


}
