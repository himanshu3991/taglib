package com.enforcers.lib.enforcerslib.logger.bean.pageview;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class NetworkDetails {

    private String networkType;
    private String ip;
}
