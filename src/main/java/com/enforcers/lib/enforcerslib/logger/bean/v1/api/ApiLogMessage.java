package com.enforcers.lib.enforcerslib.logger.bean.v1.api;

import com.enforcers.lib.enforcerslib.logger.bean.common.LogType;
import com.enforcers.lib.enforcerslib.logger.bean.v1.api.action.ApiAction;
import com.enforcers.lib.enforcerslib.logger.bean.v1.api.action.MethodAction;
import com.enforcers.lib.enforcerslib.logger.bean.v1.api.action.ServiceAction;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ApiLogMessage {

    private ApiAction api;

    private ServiceAction service;

    private MethodAction method;

    private String sessionId;

    private Long h;

    private String jsonRequest;

    private String jsonResponse;


    private String xmlRequest;

    private String xmlResponse;

    private final LogType logType = LogType.API;

}
