package com.enforcers.lib.enforcerslib.logger.bean.common;

import com.enforcers.lib.enforcerslib.logger.bean.pageview.DeviceDetails;
import com.enforcers.lib.enforcerslib.logger.bean.pageview.NetworkDetails;
import com.enforcers.lib.enforcerslib.logger.bean.pageview.SubscriptionDetails;
import com.enforcers.lib.enforcerslib.logger.bean.pageview.constant.LoginType;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class UserInfo {

    private String userId;

    private NetworkDetails networkDetails;

    private DeviceDetails deviceDetails;

    private SubscriptionDetails subscriptionDetails;

    private String geo;

    private String language;

    private String contentPreference;

    private LoginType loginType;

    private String userAgent;



    private String fingerPrintId;

    private String emailId;

    private String userAgentFamily;

    private String facebookId;

    private String googleId;

    private Long dateOfBirth;

    private String referer;

    private Boolean autoplay;

    private PlanDetails planDetails;

}
