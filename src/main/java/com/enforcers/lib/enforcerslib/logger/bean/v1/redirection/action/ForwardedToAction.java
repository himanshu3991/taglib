package com.enforcers.lib.enforcerslib.logger.bean.v1.redirection.action;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ForwardedToAction {
    private ForwardedMethod forwardedMethod;

}

@Builder
class ForwardedMethod {

    private String name;

    private String params;
}
