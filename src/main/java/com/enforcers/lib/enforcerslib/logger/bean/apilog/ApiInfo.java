package com.enforcers.lib.enforcerslib.logger.bean.apilog;

import com.enforcers.lib.enforcerslib.logger.bean.apilog.constant.HttpMethodType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApiInfo {

    private String authHeader;

    private HttpMethodType methodType;

    private String contentType;

    private String apiUrl;

    private String requestBody;

    private String headerInfo;


}
