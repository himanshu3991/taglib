package com.enforcers.lib.enforcerslib.logger.bean.common;

import com.enforcers.lib.enforcerslib.logger.bean.pageview.constant.AppType;
import com.enforcers.lib.enforcerslib.logger.bean.pageview.constant.PlatformType;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class LogMetaData {

    private AppType appType;

    private String url;

    private String appsId;

    private PlatformType platformType;


}
