package com.enforcers.lib.enforcerslib.logger.bean.apilog;

import com.enforcers.lib.enforcerslib.logger.bean.apilog.constant.Event;
import com.enforcers.lib.enforcerslib.logger.bean.apilog.constant.EventType;
import com.enforcers.lib.enforcerslib.logger.bean.common.LogMetaData;
import com.enforcers.lib.enforcerslib.logger.bean.common.UserInfo;
import com.enforcers.lib.enforcerslib.logger.bean.pageview.constant.AppType;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiLogMessage {

    @NonNull
    protected String sessionId;


    private Long currentTime;

    private String queryInfo;

    private Event event;

    private EventType eventType;

    private String networkType;

    private UserInfo userInfo;

    private LogMetaData logMetaData;

    private AppType appType;


}
