package com.enforcers.lib.enforcerslib.logger.bean.v1.event.player;

import com.enforcers.lib.enforcerslib.logger.bean.v1.event.player.action.ForwardAction;
import com.enforcers.lib.enforcerslib.logger.bean.v1.event.player.action.SkipAction;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Player {

    private ForwardAction forward;

    private SkipAction skipAction;
}
