package com.enforcers.lib.enforcerslib.logger.bean.v1.event.click.button.action;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PlayAction {

    private String id;

    private Long h;
}
