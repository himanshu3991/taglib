package com.enforcers.lib.enforcerslib.logger.bean.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PlanDetails {

    private String offerId;

    private Boolean hasOffer;

    private List<String> privileges;

    private String privilegesType;

    private String userPlanName;

    private String offerName;
}
