package com.enforcers.lib.enforcerslib.logger.bean.v1.api.action;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ServiceAction {

    private Map<String,String> requestParam;

    private String responseBody;

    private Long h;

}
