package com.enforcers.lib.enforcerslib.logger.bean.pageview.constant;

public enum Event {
	PAGE_VIEW, BUTTON, INVALID
}
