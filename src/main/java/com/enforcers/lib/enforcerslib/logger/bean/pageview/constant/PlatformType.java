package com.enforcers.lib.enforcerslib.logger.bean.pageview.constant;

public enum PlatformType {

    DESKTOP,MOBILE,TAB,WAP;
}
