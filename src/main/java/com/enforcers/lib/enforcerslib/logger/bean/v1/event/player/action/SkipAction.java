package com.enforcers.lib.enforcerslib.logger.bean.v1.event.player.action;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SkipAction {

    private Long skipTime;
}
