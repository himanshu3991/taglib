package com.enforcers.lib.enforcerslib.logger.bean.common;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SubtitlesDetails {

    private List<String> subtitlesLang;

    private String selectedSubtitle;
}
