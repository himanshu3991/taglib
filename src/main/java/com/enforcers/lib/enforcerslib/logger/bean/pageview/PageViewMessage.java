package com.enforcers.lib.enforcerslib.logger.bean.pageview;

import com.enforcers.lib.enforcerslib.logger.bean.common.LogMetaData;
import com.enforcers.lib.enforcerslib.logger.bean.common.UserInfo;
import com.enforcers.lib.enforcerslib.logger.bean.common.VideoDetails;
import com.enforcers.lib.enforcerslib.logger.bean.pageview.constant.Event;
import com.enforcers.lib.enforcerslib.logger.bean.pageview.constant.EventType;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.*;

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@Builder
@JsonInclude(Include.NON_NULL)
public class PageViewMessage {

    @NonNull
    protected String sessionId;

    private String networkType;

    private UserInfo userInfo;

    @NonNull
    protected Long currentTime;


    @NonNull
    private Event event;

    private EventType eventType;

    private String queryInfo;

    private String headerInfo;

    private LogMetaData logMetaData;

    @NonNull
    protected Long lastUpdateTime;

    @NonNull
    protected String contentId;

    private VideoDetails videoDetails;

    private String programmingId;

    private String subtitleLanguage;

    private String subPartnerName;


}
