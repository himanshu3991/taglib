package com.enforcers.lib.enforcerslib.logger.bean.v1.validation;

import com.enforcers.lib.enforcerslib.logger.bean.common.LogType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ValidatedMessage {

    private Map<String,String> headers;

    private Map<String,String> params;

    private String errorString;

    private String responseString;


    private Long h;

    private String sessionId;

    private final LogType logType =LogType.VALIDATION;

    private Boolean status;
}
