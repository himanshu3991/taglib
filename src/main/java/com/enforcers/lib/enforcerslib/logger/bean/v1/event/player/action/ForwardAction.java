package com.enforcers.lib.enforcerslib.logger.bean.v1.event.player.action;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ForwardAction {

    private Long forwardTime;
}
