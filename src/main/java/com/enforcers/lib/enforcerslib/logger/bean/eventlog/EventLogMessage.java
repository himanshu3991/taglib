package com.enforcers.lib.enforcerslib.logger.bean.eventlog;

import com.enforcers.lib.enforcerslib.logger.bean.common.ContentDetails;
import com.enforcers.lib.enforcerslib.logger.bean.common.LogMetaData;
import com.enforcers.lib.enforcerslib.logger.bean.common.UserInfo;
import com.enforcers.lib.enforcerslib.logger.bean.common.VideoDetails;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EventLogMessage {

    @NonNull
    protected String sessionId;

    private Long currentTime;

    private UserInfo userInfo;

    private LogMetaData logMetaData;

    private ContentDetails contentDetails;

    private VideoDetails videoDetails;

    private String queryInfo;

    private Long rowPosition;

    private String drmType;

    private String headerInfo;


}
