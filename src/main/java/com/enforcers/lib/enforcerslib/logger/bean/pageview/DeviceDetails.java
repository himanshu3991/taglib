package com.enforcers.lib.enforcerslib.logger.bean.pageview;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DeviceDetails {

    private String platformVersion;

    private String os;

    private String vendor;

    private String carrier;

    private String family;

    private String msidn;

}
