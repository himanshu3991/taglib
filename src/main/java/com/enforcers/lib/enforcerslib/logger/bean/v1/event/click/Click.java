package com.enforcers.lib.enforcerslib.logger.bean.v1.event.click;

import com.enforcers.lib.enforcerslib.logger.bean.v1.event.click.button.Button;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Click {

    private String id;

    private Button button;
}
