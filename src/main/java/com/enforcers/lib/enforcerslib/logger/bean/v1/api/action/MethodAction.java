package com.enforcers.lib.enforcerslib.logger.bean.v1.api.action;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MethodAction {

    private String requestParam;

    private String responseBody;

    private Long h;

}
