package com.enforcers.lib.enforcerslib.logger.bean.redirection;

import com.enforcers.lib.enforcerslib.logger.bean.apilog.ApiInfo;
import com.enforcers.lib.enforcerslib.logger.bean.common.LogMetaData;
import com.enforcers.lib.enforcerslib.logger.bean.common.UserInfo;
import lombok.*;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RedirectViewMessage {

    @NonNull
    protected String sessionId;

    private Long currentTime;

    private UserInfo userInfo;

    private LogMetaData logMetaData;

    private ApiInfo apiInfo;

    private String redirectUrl;

    private List<String> forwardedIps;

}
