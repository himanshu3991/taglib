package com.enforcers.lib.enforcerslib.logger.bean.apilog.constant;

public enum HttpMethodType {

    POST,GET,PUT,DELETE
}
