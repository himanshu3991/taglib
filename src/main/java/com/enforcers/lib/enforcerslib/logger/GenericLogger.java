package com.enforcers.lib.enforcerslib.logger;

import com.enforcers.lib.enforcerslib.logger.bean.v1.event.EventMessage;
import com.enforcers.lib.enforcerslib.logger.bean.v1.event.click.Click;
import com.enforcers.lib.enforcerslib.logger.bean.v1.event.click.button.Button;
import com.enforcers.lib.enforcerslib.logger.bean.v1.event.click.button.ButtonClickType;
import com.enforcers.lib.enforcerslib.logger.bean.v1.event.click.button.action.SubmitOtp;
import com.enforcers.lib.enforcerslib.logger.bean.v1.event.page.Page;
import com.enforcers.lib.enforcerslib.logger.bean.v1.event.page.action.CloseAction;
import com.enforcers.lib.enforcerslib.logger.bean.v1.event.page.action.Scroll;
import com.enforcers.lib.enforcerslib.logger.bean.v1.event.player.Player;
import com.enforcers.lib.enforcerslib.logger.bean.v1.event.player.action.ForwardAction;
import com.enforcers.lib.enforcerslib.logger.bean.v1.event.player.action.SkipAction;
import com.enforcers.lib.enforcerslib.logger.bean.v1.master.MasterLog;
import com.enforcers.lib.enforcerslib.logger.bean.v1.page.PageViewMessage;
import com.enforcers.lib.enforcerslib.logger.bean.v1.redirection.RedirectionLogMessage;
import com.enforcers.lib.enforcerslib.logger.bean.v1.validation.ValidatedMessage;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Slf4j
public class GenericLogger {

    private static ObjectMapper mapper;

    private GenericLogger() {
    }

    static {
        mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        JavaTimeModule javaTimeModule = new JavaTimeModule();
        javaTimeModule.addDeserializer(LocalDate.class, new LocalDateDeserializer(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        javaTimeModule.addSerializer(LocalDate.class, new LocalDateSerializer(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        mapper.registerModule(javaTimeModule);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }


    //-----------------------------------------------------------------------------------------------------------------------
    public static void log(EventMessage eventMessage) {
        try {
            log.info(mapper.writeValueAsString(eventMessage));
        } catch (JsonProcessingException e) {
            System.err.println("JsonProcessingException for pubsub pageViewMessage. + " + e.getMessage());
        }
    }



    public static void log(com.enforcers.lib.enforcerslib.logger.bean.v1.api.ApiLogMessage eventMessage) {
        try {

            log.info(mapper.writeValueAsString(eventMessage));
        } catch (JsonProcessingException e) {
            System.err.println("JsonProcessingException for pubsub pageViewMessage. + " + e.getMessage());
        }
    }

    public static void log(RedirectionLogMessage eventMessage) {
        try {

            log.info(mapper.writeValueAsString(eventMessage));
        } catch (JsonProcessingException e) {
            System.err.println("JsonProcessingException for pubsub pageViewMessage. + " + e.getMessage());
        }
    }

    public static void log(ValidatedMessage eventMessage) {
        try {

            log.info(mapper.writeValueAsString(eventMessage));
        } catch (JsonProcessingException e) {
            System.err.println("JsonProcessingException for pubsub pageViewMessage. + " + e.getMessage());
        }
    }

    public static void log(MasterLog eventMessage) {
        try {

            log.info(mapper.writeValueAsString(eventMessage));
        } catch (JsonProcessingException e) {
            System.err.println("JsonProcessingException for pubsub pageViewMessage. + " + e.getMessage());
        }
    }
    public static void log(PageViewMessage eventMessage) {
        try {

            log.info(mapper.writeValueAsString(eventMessage));
        } catch (JsonProcessingException e) {
            System.err.println("JsonProcessingException for pubsub pageViewMessage. + " + e.getMessage());
        }
    }

}
