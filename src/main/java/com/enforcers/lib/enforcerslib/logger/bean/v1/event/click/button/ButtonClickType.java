package com.enforcers.lib.enforcerslib.logger.bean.v1.event.click.button;

public enum ButtonClickType {
    SUBMIT, CLOSE, MINIMISED
}

