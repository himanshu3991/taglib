package com.enforcers.lib.enforcerslib.logger.bean.v1.event;

import com.enforcers.lib.enforcerslib.logger.bean.common.LogType;
import com.enforcers.lib.enforcerslib.logger.bean.v1.event.click.Click;
import com.enforcers.lib.enforcerslib.logger.bean.v1.event.page.Page;
import com.enforcers.lib.enforcerslib.logger.bean.v1.event.page.action.Scroll;
import com.enforcers.lib.enforcerslib.logger.bean.v1.event.player.Player;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EventMessage {

    private String sessionId;

    private Click click;

    private Player player;

    private Page page;

    private Long h;

    private final LogType logType =LogType.EVENT;
}

