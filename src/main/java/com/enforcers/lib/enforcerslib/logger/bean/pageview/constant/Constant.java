package com.enforcers.lib.enforcerslib.logger.bean.pageview.constant;

public class Constant {
    public static final String INVALID_CONTENT_ID = "INVALID";
    public static final long DEFAULT_LAST_UPDATE_TIME = 0;
    public static final String VUCLIP_PACKAGE = "com.vuclip";
    public static final String NEW_LINE_CHAR = "\n";
}
