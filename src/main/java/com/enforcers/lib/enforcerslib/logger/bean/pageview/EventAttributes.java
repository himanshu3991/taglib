package com.enforcers.lib.enforcerslib.logger.bean.pageview;

import com.enforcers.lib.enforcerslib.logger.bean.pageview.constant.Event;
import com.enforcers.lib.enforcerslib.logger.bean.pageview.constant.EventType;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.*;

@Data
@Builder
@JsonInclude(Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
public class EventAttributes {

    @NonNull
    private Event event;

    private EventType eventType;

}
