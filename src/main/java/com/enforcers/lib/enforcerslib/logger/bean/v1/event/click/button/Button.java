package com.enforcers.lib.enforcerslib.logger.bean.v1.event.click.button;

import com.enforcers.lib.enforcerslib.logger.bean.v1.event.click.button.action.SubmitOtp;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Button {

    private String name;

    private ButtonClickType type;

    private SubmitOtp otp;
}

