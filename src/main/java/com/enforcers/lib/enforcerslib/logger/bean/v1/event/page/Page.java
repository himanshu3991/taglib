package com.enforcers.lib.enforcerslib.logger.bean.v1.event.page;

import com.enforcers.lib.enforcerslib.logger.bean.v1.event.page.action.CloseAction;
import com.enforcers.lib.enforcerslib.logger.bean.v1.event.page.action.Scroll;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Page {

    private String url;

    private Scroll scroll;

    private CloseAction close;


}
