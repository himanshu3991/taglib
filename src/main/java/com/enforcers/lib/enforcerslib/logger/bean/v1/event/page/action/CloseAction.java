package com.enforcers.lib.enforcerslib.logger.bean.v1.event.page.action;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CloseAction {

    private String id;

    private String name;
}
