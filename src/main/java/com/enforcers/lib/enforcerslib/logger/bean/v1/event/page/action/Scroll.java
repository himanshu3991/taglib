package com.enforcers.lib.enforcerslib.logger.bean.v1.event.page.action;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Scroll {

    private Long id;

    private Long h;
}
