package com.enforcers.lib.enforcerslib.logger.bean.v1.api.action;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ApiAction {

    private String api;

    private String requestBody;

    private String responseBody;

    private String sessionId;

    private Long h;
}
