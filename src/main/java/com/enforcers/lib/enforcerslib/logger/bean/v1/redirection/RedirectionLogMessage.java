package com.enforcers.lib.enforcerslib.logger.bean.v1.redirection;

import com.enforcers.lib.enforcerslib.logger.bean.common.LogType;
import com.enforcers.lib.enforcerslib.logger.bean.v1.redirection.action.ForwardedToAction;
import com.enforcers.lib.enforcerslib.logger.bean.v1.redirection.action.RedirectionToAction;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RedirectionLogMessage {

    private RedirectionToAction redirection;

    private ForwardedToAction forwarded;

    private String sessionId;

    private Long h;

    private final LogType logType =LogType.RE_DIRECTION;
}
